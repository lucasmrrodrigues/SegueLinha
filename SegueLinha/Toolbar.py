import pygame

class Toolbar:
	def __init__(self, width, height): 
		self.image = pygame.Surface((width, height))
		self.image.fill((100,100,100))
		self.rect = self.image.get_rect()
		self.rect.topleft = (0,0)

		self.leftButton = LeftButton((height, height))
		self.playButton = PlayButton((height, height))
		self.stopButton = StopButton((height, height))
		self.resetButton = ResetButton((height, height))
		self.rightButton = RightButton((height, height))

	def draw(self, screen):
		screen.blit(self.image, self.rect)

		self.leftButton.draw(screen, (0,0))
		self.playButton.draw(screen, (50,0))
		self.stopButton.draw(screen, (100,0))
		self.resetButton.draw(screen, (150,0))
		self.rightButton.draw(screen, (200,0))

	def click(self, pos):
		if self.playButton.collidepoint(pos):
			return 'play'

		if self.stopButton.collidepoint(pos):
			return 'stop'

		if self.leftButton.collidepoint(pos):
			return 'left'

		if self.rightButton.collidepoint(pos):
			return 'right'

		if self.resetButton.collidepoint(pos):
			return 'reset'

		return None

class BaseButton:
	def draw(self, screen, pos):
		screen.blit(self.sprite,pos)
		self.rect = (pos[0],pos[1],self.sprite.get_width(),self.sprite.get_height())

	def collidepoint(self, pos):
		return pos[0] >= self.rect[0] and pos[0] <= self.rect[0]+self.rect[2] and pos[1] >= self.rect[1] and pos[1] <= self.rect[1]+self.rect[3]

class PlayButton(BaseButton):
	def __init__(self, size):
		self.sprite = pygame.image.load("ToolbarImages/play.png")
		self.sprite = pygame.transform.scale(self.sprite, size)

class StopButton(BaseButton):
	def __init__(self, size):
		self.sprite = pygame.image.load("ToolbarImages/stop.png")
		self.sprite = pygame.transform.scale(self.sprite, size)

class LeftButton(BaseButton):
	def __init__(self, size):
		self.sprite = pygame.image.load("ToolbarImages/left.png")
		self.sprite = pygame.transform.scale(self.sprite, size)

class RightButton(BaseButton):
	def __init__(self, size):
		self.sprite = pygame.image.load("ToolbarImages/right.png")
		self.sprite = pygame.transform.scale(self.sprite, size)

class ResetButton(BaseButton):
	def __init__(self, size):
		self.sprite = pygame.image.load("ToolbarImages/reset.png")
		self.sprite = pygame.transform.scale(self.sprite, size)
