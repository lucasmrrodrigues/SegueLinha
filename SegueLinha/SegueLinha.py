import pygame
from Robot import Robot
import RobotControl
import sys
import math
from pygame import gfxdraw
from Toolbar import *
import importlib
import os


screen_size = width, height = 800, 600
black = 0, 0, 0
white = 255, 255, 255
blue = 0, 0, 255
green = 0, 255, 0
red = 255, 0, 0
yellow = 255, 255, 0
gray = 200, 200, 200
FPS = 20
Fcont = 0

class Environment(object):
	def __init__(self):
		self.toolbarHeight = 100
		self.screen = pygame.display.set_mode((width, height + self.toolbarHeight))
		pygame.init()
		
		self.defaultPos = (300, 150)

		self.timestamp = 0
		self.finish = False
		self.key = ['']
		self.currentTrack = 0
		self.loadTrack(self.currentTrack)
		self.robot = Robot(self.defaultPos, self.key)
		self.control = RobotControl.RobotControl(self.robot)
		self.running = False
		self.textfont = pygame.font.SysFont("Arial", 40)
		self.scanRange = 101
		self.toolbar = Toolbar(width,50)

	def getTrack(self, index):
		tracks = os.listdir('./Tracks')
		index = index%len(tracks)
		if index < 0:
			index+=len(tracks)
		track = tracks[index]
		self.background = pygame.image.load("Tracks/"+track)
		details = track.split('_')
		if len(details)==2:
			pos = details[1].split('x')
			if len(pos)==2:
				pos2 = pos[1].split('.')
				if len(pos2)==2:
					try:
						self.defaultPos = (int(pos[0]), int(pos2[0]))
					except:
						self.defaultPos = (300, 150)
			else:
				self.defaultPos = (300, 150)
		else:
			self.defaultPos = (300, 150)
	
	def loadTrack(self, index):
		self.getTrack(index)
		self.totalPixels = 0
		self.coveredPixels = 0
		for i in range(self.background.get_width()):
			for j in range(self.background.get_height()):
				if self.background.get_at((i,j)) != white:
					self.totalPixels += 1
                                        

	def draw(self):
		self.screen.fill(white)
		self.drawEnvironment()
		self.drawDefaultPosition()
		self.drawRobot()
		self.drawScanner()
		self.toolbar.draw(self.screen)
		self.drawTimestamp()
		self.drawPercentage()
		pygame.display.flip()
		if self.running and not self.finished():
			self.timestamp += 1/FPS

		self.getEvents()

	def drawDefaultPosition(self):
		pygame.draw.circle(self.screen, blue, self.defaultPos, 5)

	def finished(self):
		if self.finish:
			return True
		robotX = self.robot.getX(self.key)
		robotY = self.robot.getY(self.key)
		percentageCovered = self.coveredPixels/self.totalPixels
		if percentageCovered >= 0.95:
			dx = self.defaultPos[0]-robotX
			dy = self.defaultPos[1]-robotY
			if dx*dx+dy*dy <= 625:
				self.finish = True
				return True
		return False

	def drawEnvironment(self):
		self.screen.blit(self.background, (0,self.toolbarHeight))

	def drawRobot(self):
		self.robot.update(1/FPS, self.key)
		surf =  self.robot.getSprite(self.key)
		surf = pygame.transform.rotate(surf, self.robot.getAng(self.key))
		where = (self.robot.getX(self.key)-surf.get_width()/2,self.robot.getY(self.key)-surf.get_height()/2)
		self.screen.blit(surf, where)

	def drawTimestamp(self):
		label = self.textfont.render("Timestamp: " + "{0:.2f}".format(self.timestamp) + "s", True, black) # test line scanner
		self.screen.blit(label, (width - label.get_width()-10, 1))

	def drawPercentage(self):
		percentageCovered = self.coveredPixels/self.totalPixels * 100
		label = self.textfont.render("{0:.2f}".format(percentageCovered) + "%", True, black) # test line scanner
		self.screen.blit(label, (width / 2 - label.get_width() / 2, 1))

	def drawScanner(self):
		p0 = self.robot.getX(self.key) + math.cos(math.radians(self.robot.getAng(self.key)))*(self.robot.get_scan_distance()+self.robot.getSprite(self.key).get_width()/2), self.robot.getY(self.key) - math.sin(math.radians(self.robot.getAng(self.key)))*(self.robot.get_scan_distance()+self.robot.getSprite(self.key).get_width()/2)

		p1 = p0[0] - math.sin(math.radians(self.robot.getAng(self.key)))*self.robot.getSprite(self.key).get_width()/2, p0[1] - math.cos(math.radians(self.robot.getAng(self.key)))*self.robot.getSprite(self.key).get_width()/2

		p2 = p1[0] + math.cos(math.radians(self.robot.getAng(self.key)))*5, p1[1] - math.sin(math.radians(self.robot.getAng(self.key)))*5

		p3 = p0[0] + math.sin(math.radians(self.robot.getAng(self.key)))*self.robot.getSprite(self.key).get_width()/2, p0[1] + math.cos(math.radians(self.robot.getAng(self.key)))*self.robot.getSprite(self.key).get_width()/2

		p4 = p3[0] + math.cos(math.radians(self.robot.getAng(self.key)))*5, p3[1] - math.sin(math.radians(self.robot.getAng(self.key)))*5

		t = (p1,p2,p4,p3)

		color = red

		x = p1[0]
		y = p1[1]

		line = 0
		count = 0

		x_inc = (p3[0] - p1[0])/self.scanRange
		y_inc = (p3[1] - p1[1])/self.scanRange

		for i in range(self.scanRange):
			try:
				pixel = self.screen.get_at((int(x),int(y)))
			except Exception as e:
				pixel = [255,255,255]

			if (pixel[0], pixel[1], pixel[2]) != white:
				line+=i
				count+=1
			#pygame.draw.line(self.screen, green, (int(x), int(y)), (int(x), int(y)))
			x += x_inc
			y += y_inc

		robotX = int(self.robot.getX(self.key))
		robotY = int(self.robot.getY(self.key)) - self.toolbarHeight
		for i in range(-25, 26):
			for j in range(-25, 26):
				if i*i+j*j<=625:
					try:
						pixel = self.background.get_at((robotX+i, robotY+j))
						if pixel != white and pixel != green:
							self.background.set_at((robotX+i, robotY+j), green)
							self.coveredPixels+=1
					except:
						pass

		if count == 0:
			line = None
		else:
			line /= count
			line -= (self.scanRange-1)/2
			
		self.robot.setLine(line, self.key)

		gfxdraw.filled_polygon(self.screen, t, color)
		gfxdraw.aapolygon(self.screen, t, black)

	def reset(self):
		self.running = False
		self.control.running = False
		if self.control.isAlive():
			self.control.join()
		self.loadTrack(self.currentTrack)
		self.robot = Robot(self.defaultPos, self.key)
		self.control = RobotControl.RobotControl(self.robot)
		self.timestamp = 0
		self.finish = False

	def getEvents(self):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()
			if event.type == pygame.MOUSEBUTTONUP:
				button = self.toolbar.click(pygame.mouse.get_pos())
				if button != None:
					importlib.reload(RobotControl)
					if button == 'play':
						if not self.running:
							self.control = RobotControl.RobotControl(self.robot)
							self.running = True
							self.control.start()
					elif button == 'stop':
						if self.running:
							self.running = False
							self.control.running = False
							self.control.join()
							self.robot.stop(self.key)
							self.control = RobotControl.RobotControl(self.robot)
					elif button == 'reset':
						self.reset()
					elif button == 'left':
						self.currentTrack-=1
						self.reset()
					elif button == 'right':
						self.currentTrack+=1
						self.reset()

env = Environment()
clock = pygame.time.Clock()
pygame.time.wait(1000)
while True:
	clock.tick(FPS)
	env.draw()
