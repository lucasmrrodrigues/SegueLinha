import pygame
import math
from random import *

class Robot:

	def __init__(self, pos, key):

		self.__key = str(randint(0,999999))
		key[0] = self.__key

		# sprite for the robot
		self.__sprite = pygame.image.load("Images/robot.png")
		
		self.__sprite = pygame.transform.scale(self.__sprite, (50,50))
		texto = pygame.image.tostring(self.__sprite, "RGBA")
		
		# x and y position on screen
		'''
		  x - >
		y --------
		| |
		V |
		'''
		self.__x = pos[0]
		self.__y = pos[1]
		
		# direction of the robot in degrees
		'''
		       90º
				^
				|
		180º <-- --> 0º
		        |
				V
			-90º/270º
		'''
		self.__ang = 0

		# acceleration of the robot in pixels/s²
		self.__lin_acc = 100

		# angular acceleration of the robot in degrees/s²
		self.__ang_acc = 100

		# actual velocities of the robot in pixels/s and degrees/s
		self.__lin_vel = 0
		self.__ang_vel = 0

		# set point velocities set by controller in pixels/s and degrees/s
		self.__lin_vel_sp = 0
		self.__ang_vel_sp = 0

		# maximum velocities
		self.__vmax = 200
		self.__vangmax = 90

		self.__scan_distance = 10
		
		self.__max_scan_distance = 50

		self.__line = 0

	def update(self, time, key):
		if key[0] == self.__key:
			# time is how has passed since last update (1/FPS)

			# update velocities based on set_point and acceleration
			self.__lin_vel += min(abs(self.__lin_vel_sp - self.__lin_vel), self.__lin_acc*time)* (-1 if self.__lin_vel_sp < self.__lin_vel else 1)
			self.__ang_vel += min(abs(self.__ang_vel_sp - self.__ang_vel), self.__ang_acc*time)* (-1 if self.__ang_vel_sp < self.__ang_vel else 1)

			# update direction and position of the robot based on velocities
			self.__ang += (self.__ang_vel*time)%360
			self.__x += self.__lin_vel*time*math.cos(math.radians(self.__ang))
			self.__y -= self.__lin_vel*time*math.sin(math.radians(self.__ang))
	


	def getSprite(self, key):
		if key[0] == self.__key:
			return self.__sprite

	def stop(self, key):
		if key[0] == self.__key:
			self.__lin_vel_sp = 0
			self.__ang_vel_sp = 0
			self.__lin_vel = 0
			self.__ang_vel = 0

	def getX(self, key):
		if key[0] == self.__key:
			return self.__x

	def getY(self, key):
		if key[0] == self.__key:
			return self.__y

	def getAng(self, key):
		if key[0] == self.__key:
			return self.__ang

	def setLine(self, line, key):
		if key[0] == self.__key:
			self.__line = line

	def get_scan_distance(self):
		return self.__scan_distance

	# public ----------------------------------------------------------------

	def set_linear_velocity(self, vel):
		self.__lin_vel_sp = max(min(self.__vmax,vel),0)

	def set_angular_velocity(self, vel):
		self.__ang_vel_sp = max(min(self.__vangmax,vel),-self.__vangmax)

	def set_scanner_distance(self, dist):
		self.__scan_distance = max(min(self.__max_scan_distance,dist),0)

	def get_scan_read(self):
		return self.__line
