from threading import Thread
import sys
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import time

class RobotControl(Thread):
	def __init__(self, robot):
		self.robot = robot
		Thread.__init__(self)
		self.running = False

	'''
		Métodos disponíveis:

		self.robot.set_linear_velocity(vel) = ajusta velocidade para frente do robo
		self.robot.set_angular_velocity(ang) = ajusta velocidade angular, < 0 horário, > 0 anti-horário
		self.robot.set_scanner_distance(dist) = ajusta distância do sensor de linha para o robô
		self.robot.get_scan_read() = le o scanner de linha, -50 (esquerda) até +50(direita)
	'''

	def run(self):
		
		self.running = True

		self.robot.set_scanner_distance(20)

		vel = [
			(-50,20),
			(-40,70),
			(-5,100),
			(0,150),
			(5,100),
			(40,70),
			(50,20)
			]

		ang = [
			(-50,90),
			(-40,50),
			(-10,20),
			(0,0),
			(10,-20),
			(40,-50),
			(50,-90)
			]

		line = None

		while self.running:
			oldline = line
			line = self.robot.get_scan_read()

			if line == None and oldline != None:
				line = -50 if oldline < 0 else 50

			if line != None:
				self.robot.set_linear_velocity(interpola(vel,line))
				self.robot.set_angular_velocity(interpola(ang,line))


			time.sleep(0.01)

def interpola(func, ent):
	qtd = len(func)

	if ent <= func[0][0]:
		return func[0][1]
	elif ent >= func[qtd-1][0]:
		return func[qtd-1][1]
	else:
		i = 0
		while func[i][0]<ent:
			i+= 1

		return ((ent-func[i-1][0])/(func[i][0] - func[i-1][0]))*(func[i][1]-func[i-1][1])+func[i-1][1]


