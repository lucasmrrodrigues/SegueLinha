from threading import Thread
import sys
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import time

class RobotControl(Thread):
        def __init__(self, robot):
                self.robot = robot
                Thread.__init__(self)
                self.running = False

        '''
        O objetivo deste projeto é fazer o robô completar uma volta na pista.
        Um volta é considerada completa quando ao menos 95% da pista
        tenha sido respeitada e o robô tenha passado pelo ponto de partida novamente.
        Para executar o programa, execute o arquivo "SegueLinha.py".

        A função run abaixo deve ser alterada para utilizar o pacote skfuzzy.
        
        O valor de entrada do fuzzy será o resultado da função get_scan_read
        descrita abaixo.
        
        Os valores de saída serão utilizados para chamar as funções
        set_linear_velocity e set_angular_velocity descritas abaixo.

        Métodos disponíveis:

                self.robot.get_scan_read():
                        Lê o scanner de linha.
                        Retorna valores entre -50 (esquerda) até +50(direita).
                        Retorna None quando o scanner não encontra a pista.
                        
                self.robot.set_linear_velocity(vel):
                        Ajusta velocidade para frente do robô (pixels/s).
                        Aceita valores entre 0 e 200.
                        
                self.robot.set_angular_velocity(ang):
                        Ajusta velocidade angular, < 0 horário, > 0 anti-horário (graus/s).
                        Aceita valores entre -90 e 90.
                        
                self.robot.set_scanner_distance(dist):
                        Ajusta distância do sensor de linha para o robô.
                        Aceita valores entre 0 e 50.
        '''

        def run(self):

                ''' A estrutura da função deve ser mantida'''
                self.running = True #Não alterar
                self.robot.set_scanner_distance(10) # Se quiser, alterar a distância do sensor de linha.

                line = ctrl.Antecedent(np.arange(-50, 51, 1), 'line')
                vel = ctrl.Consequent(np.arange(0, 201, 1), 'vel')
                angvel = ctrl.Consequent(np.arange(-90, 91, 1), 'angvel')
                dist = ctrl.Consequent(np.arange(0, 51, 1), 'dist')

                line['<---'] = fuzz.trimf(line.universe, [-50, -50, -40])
                line['<--'] = fuzz.trimf(line.universe, [-50, -40, -30])
                line['<-'] = fuzz.trimf(line.universe, [-40, -30, 0])
                line['<'] = fuzz.trimf(line.universe, [-20, 0, 0])

                line['|'] = fuzz.trimf(line.universe, [0, 0, 0])

                line['--->'] = fuzz.trimf(line.universe, [40, 50, 50])
                line['-->'] = fuzz.trimf(line.universe, [30, 40, 50])
                line['->'] = fuzz.trimf(line.universe, [0, 30, 40])
                line['>'] = fuzz.trimf(line.universe, [0, 0, 20])

                vel['0'] = fuzz.trimf(vel.universe, [10,20,20])
                vel['^'] = fuzz.trimf(vel.universe, [20,20,40])
                vel['^^'] = fuzz.trimf(vel.universe, [50,70,100])
                vel['^^^'] = fuzz.trimf(vel.universe, [50,70,150])
                vel['^^^^'] = fuzz.trimf(vel.universe, [200,200,200])

                angvel['<---'] = fuzz.trimf(angvel.universe, [70, 90, 90])
                angvel['<--'] = fuzz.trimf(angvel.universe, [30, 70, 90])
                angvel['<-'] = fuzz.trimf(angvel.universe, [10, 30, 70])
                angvel['<'] = fuzz.trimf(angvel.universe, [0, 0, 10])

                angvel['|'] = fuzz.trimf(angvel.universe, [0, 0, 0])

                angvel['--->'] = fuzz.trimf(angvel.universe, [-90, -90, -70])
                angvel['-->'] = fuzz.trimf(angvel.universe, [-90, -70, -30])
                angvel['->'] = fuzz.trimf(angvel.universe, [-70, -30, -10])
                angvel['>'] = fuzz.trimf(angvel.universe, [-10, 0, 0])


                dist['0'] = fuzz.trimf(dist.universe, [5,5,5])
                dist['^'] = fuzz.trimf(dist.universe, [5,10,20])
                dist['^^'] = fuzz.trimf(dist.universe, [10,20,30])
                dist['^^^'] = fuzz.trimf(dist.universe, [30,40,40])
                dist['^^^^'] = fuzz.trimf(dist.universe, [50,50,50])
                
                rules = []

                rules.append(ctrl.Rule(line['|'], vel['^^^^']))
                rules.append(ctrl.Rule(line['|'], angvel['|']))

                rules.append(ctrl.Rule(line['<'], vel['^^^']))
                rules.append(ctrl.Rule(line['>'], vel['^^^']))
                rules.append(ctrl.Rule(line['<'], angvel['<']))
                rules.append(ctrl.Rule(line['>'], angvel['>']))

                rules.append(ctrl.Rule(line['<-'], vel['^^']))
                rules.append(ctrl.Rule(line['->'], vel['^^']))
                rules.append(ctrl.Rule(line['<-'], angvel['<-']))
                rules.append(ctrl.Rule(line['->'], angvel['->']))

                rules.append(ctrl.Rule(line['<--'], vel['^']))
                rules.append(ctrl.Rule(line['-->'], vel['^']))
                rules.append(ctrl.Rule(line['<--'], angvel['<--']))
                rules.append(ctrl.Rule(line['-->'], angvel['-->']))

                rules.append(ctrl.Rule(line['<---'], vel['0']))
                rules.append(ctrl.Rule(line['--->'], vel['0']))
                rules.append(ctrl.Rule(line['<---'], angvel['<---']))
                rules.append(ctrl.Rule(line['--->'], angvel['--->']))

                rules.append(ctrl.Rule(line['|'], dist['^^^^']))
                rules.append(ctrl.Rule(line['<'], dist['^^^']))
                rules.append(ctrl.Rule(line['>'], dist['^^^']))
                rules.append(ctrl.Rule(line['<-'], dist['^^']))
                rules.append(ctrl.Rule(line['->'], dist['^^']))
                rules.append(ctrl.Rule(line['<--'], dist['^']))
                rules.append(ctrl.Rule(line['-->'], dist['^']))
                rules.append(ctrl.Rule(line['<---'], dist['0']))
                rules.append(ctrl.Rule(line['--->'], dist['0']))

                robot_control = ctrl.ControlSystemSimulation(ctrl.ControlSystem(rules))

                oldline = None
                
                while self.running: #Não alterar
                        oldline = line
                        line = self.robot.get_scan_read() #Não alterar

                        if line == None and oldline != None:
                                line = -50 if oldline < 0 else 50
                        
                        if line != None: # Não alterar
                                try:
                                        robot_control.input['line'] = line

                                        robot_control.compute()

                                        self.robot.set_scanner_distance(robot_control.output['dist'])
                                        self.robot.set_linear_velocity(robot_control.output['vel'])
                                        self.robot.set_angular_velocity(robot_control.output['angvel'])
                                        
                                except:
                                        print("Error com line: ", line)
                                
                        time.sleep(0.01) # Não alterar
                        
